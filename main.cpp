#include <iostream>
#include <string>

int main()
{
	std::string Hi ("Hello, Artyom!");

	std::cout << Hi << "\n";
	std::cout << Hi.length() << "\n";
	std::cout << Hi[0] << "\n";
	std::cout << Hi[13] << "\n";

	return 0;
}